pub use crate::camera::Camera;
pub use crate::renderer::Renderer;

mod bounding_volume;

mod camera;

pub mod color;

pub mod math {
	use nalgebra::{Point3, Vector3, U3};

	pub use super::real::*;
	//pub use super::simd::*;

	pub const DIM: usize = 3;

	pub type Dim = U3;

	pub type Point<N> = Point3<N>;

	pub type Vector<N> = Vector3<N>;
}

mod ray;

mod real {
	pub type Real = f64;
}

mod renderer;

pub mod shape;

/*mod simd {
	use simba::simd::{AutoBoolx4, AutoF64x4};

	pub const SIMD_WIDTH: usize = 4;

	pub const SIMD_LAST_INDEX: usize = 3;

	pub type SimdReal = AutoF64x4;

	pub type SimdBool = AutoBoolx4;
}*/
