use crate::math::{Point, Real, Vector};
use crate::ray::Ray;
use nalgebra::Unit;

#[derive(Clone, Copy, Debug)]
pub struct Camera {
	horizontal: Vector<Real>,
	lower_left_corner: Vector<Real>,
	origin: Point<Real>,
	vertical: Vector<Real>,
}

impl Camera {
	pub fn get_ray(&self, s: Real, t: Real) -> Ray {
		Ray::new(
			self.origin,
			self.lower_left_corner + s * self.horizontal + t * self.vertical - self.origin.coords,
		)
	}

	pub fn new(
		aspect_ratio: Real,
		look_at: Point<Real>,
		look_from: Point<Real>,
		v_fov: Real,
		v_up: Unit<Vector<Real>>,
	) -> Self {
		let viewport_height = 2.0 * (v_fov.to_radians() / 2.0).tan();
		let viewport_width = aspect_ratio * viewport_height;

		let w = (look_from - look_at).normalize();
		let u = (v_up.cross(&w)).normalize();
		let v = w.cross(&u);

		let horizontal = viewport_width * u;
		let vertical = viewport_height * v;
		Self {
			horizontal,
			lower_left_corner: look_from.coords - horizontal / 2.0 - vertical / 2.0 - w,
			origin: look_from,
			vertical,
		}
	}
}
