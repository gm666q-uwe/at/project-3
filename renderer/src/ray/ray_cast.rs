use crate::math::Real;
use crate::ray::{Ray, RayIntersection};

pub trait RayCast {
	fn cast_ray(&self, ray: &Ray, min_toi: Real, max_toi: Real) -> Option<RayIntersection>;
}
