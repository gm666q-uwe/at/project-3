use crate::math::{Point, Real, Vector};

#[derive(Clone, Copy, Debug)]
pub struct Ray {
	pub origin: Point<Real>,
	pub direction: Vector<Real>,
}

impl Ray {
	#[inline]
	pub fn direction(&self) -> &Vector<Real> {
		&self.direction
	}

	pub fn new(origin: Point<Real>, direction: Vector<Real>) -> Self {
		Self { origin, direction }
	}

	#[inline]
	pub fn origin(&self) -> &Point<Real> {
		&self.origin
	}

	#[inline]
	pub fn point_at(&self, t: Real) -> Point<Real> {
		self.origin + self.direction * t
	}
}
