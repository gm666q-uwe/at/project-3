use crate::math::{Real, Vector};

#[derive(Clone, Copy, Debug)]
pub struct RayIntersection {
	toi: Real,
	normal: Vector<Real>,
	front_face: bool,
}

impl RayIntersection {
	#[inline]
	pub fn front_face(&self) -> bool {
		self.front_face
	}

	pub fn new(toi: Real, normal: Vector<Real>, front_face: bool) -> Self {
		RayIntersection {
			toi,
			normal,
			front_face,
		}
	}

	#[inline]
	pub fn normal(&self) -> &Vector<Real> {
		&self.normal
	}

	#[inline]
	pub fn toi(&self) -> Real {
		self.toi
	}
}
