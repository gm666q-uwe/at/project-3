use crossbeam::thread;
use nalgebra::Vector4;
use num_traits::Float;
use rand::distributions::uniform::SampleRange;
use rand::rngs::ThreadRng;
use rand::{thread_rng, Rng};

use crate::math::{Real, Vector};
use crate::ray::{Ray, RayCast};
use crate::shape::World;
use crate::Camera;

pub struct Renderer {
	width: usize,
	height: usize,
	samples: usize,
	threads: usize,
}

impl Renderer {
	const MAX_DEPTH: usize = 50;

	pub fn new(width: usize, height: usize, samples: usize, threads: usize) -> Self {
		Renderer {
			width,
			height,
			samples,
			threads,
		}
	}

	fn ray_color(depth: usize, rng: &mut ThreadRng, ray: &Ray, world: &World) -> Vector<Real> {
		if depth <= 0 {
			return Vector::new(0.0, 0.0, 0.0);
		}

		if let Some(intersection) = world.cast_ray(ray, 0.001 /*Real::epsilon()*/, Real::INFINITY) {
			let p = ray.point_at(intersection.toi());
			let target = p + intersection.normal() + Self::random_inside_hemisphere(intersection.normal(), rng);
			return 0.5 * Self::ray_color(depth - 1, rng, &Ray::new(p, target - p), world);
		}

		let unit_direction = ray.direction.normalize();
		let t = 0.5 * (unit_direction.y + 1.0);
		return (1.0 - t) * Vector::new(1.0, 1.0, 1.0) + t * Vector::new(0.5, 0.7, 1.0);
	}

	pub fn render(&self, camera: &Camera, world: &World) -> Vec<Vector4<Real>> {
		let height_sub_one = self.height as Real - 1.0;
		let width_sub_one = self.width as Real - 1.0;

		let len_per_thread = (self.height / self.threads) * self.width;

		let mut result = vec![Vector4::new(0.0, 0.0, 0.0, 0.0); self.width * &self.height];

		thread::scope(|s| {
			let mut count = 0;
			let mut height_offset = 0;
			for (index, slice) in result
				.split_inclusive_mut(|_| {
					count += 1;
					if count == len_per_thread {
						count = 0;
						return true;
					}
					return false;
				})
				.enumerate()
			{
				let height = slice.len() / self.width;
				s.spawn(move |_| {
					let mut rng = thread_rng();
					for j in (0..height).rev() {
						for i in 0..self.width {
							let mut color = Vector::new(0.0, 0.0, 0.0);
							for s in 0..self.samples {
								let u = (i as Real + rng.gen_range(0.0..=1.0)) / width_sub_one;
								let v = ((height_sub_one - (j + height_offset) as Real) + rng.gen_range(0.0..=1.0))
									/ height_sub_one;
								color += Self::ray_color(Self::MAX_DEPTH, &mut rng, &camera.get_ray(u, v), world);
							}
							slice[j * self.width + i] = Self::write_color(color, self.samples);
						}
					}
				});
				height_offset += height;
			}
		})
		.unwrap();

		result
	}

	fn random_inside_hemisphere(normal: &Vector<Real>, rng: &mut ThreadRng) -> Vector<Real> {
		let inside_unit_sphere = Self::random_inside_unit_sphere(rng);
		if inside_unit_sphere.dot(normal) > 0.0 {
			inside_unit_sphere
		} else {
			-inside_unit_sphere
		}
	}

	fn random_inside_unit_sphere(rng: &mut ThreadRng) -> Vector<Real> {
		loop {
			let p = Self::random_vector_range(rng, -1.0..1.0);
			if p.magnitude_squared() >= 1.0 {
				continue;
			}
			return p;
		}
	}

	fn random_vector(rng: &mut ThreadRng) -> Vector<Real> {
		Vector::new(rng.gen(), rng.gen(), rng.gen())
	}

	fn random_vector_range<R>(rng: &mut ThreadRng, range: R) -> Vector<Real>
	where
		R: Clone + SampleRange<Real>,
	{
		Vector::new(
			rng.gen_range(range.clone()),
			rng.gen_range(range.clone()),
			rng.gen_range(range.clone()),
		)
	}

	fn write_color(color: Vector<Real>, samples: usize) -> Vector4<Real> {
		let scale = 1.0 / samples as Real;
		Vector4::new(
			(color.x * scale).sqrt(),
			(color.y * scale).sqrt(),
			(color.z * scale).sqrt(),
			1.0,
		)
	}
}
