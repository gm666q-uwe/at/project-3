pub trait BoundingVolume {
	fn merge(&mut self, _: &Self);

	fn merged(&self, _: &Self) -> Self;
}
