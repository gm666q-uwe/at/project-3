use num_traits::identities::Zero;

use crate::bounding_volume::BoundingVolume;
use crate::math::{Point, Real};

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct BoundingSphere {
	center: Point<Real>,
	radius: Real,
}

impl BoundingSphere {
	#[inline]
	pub fn center(&self) -> &Point<Real> {
		&self.center
	}

	pub fn new(center: Point<Real>, radius: Real) -> Self {
		BoundingSphere { center, radius }
	}

	#[inline]
	pub fn radius(&self) -> Real {
		self.radius
	}
}

impl BoundingVolume for BoundingSphere {
	#[inline]
	fn merge(&mut self, other: &Self) {
		let mut dir = other.center() - self.center();

		if dir.normalize_mut().is_zero() {
			if other.radius() > self.radius() {
				self.radius = other.radius()
			}
		} else {
			let self_center_dir = self.center().coords.dot(&dir);
			let other_center_dir = other.center().coords.dot(&dir);

			let right;
			let left;

			if self_center_dir + self.radius() > other_center_dir + other.radius() {
				right = self.center() + dir * self.radius();
			} else {
				right = other.center() + dir * other.radius();
			}

			if -self_center_dir + self.radius() > -other_center_dir + other.radius() {
				left = self.center() - dir * self.radius();
			} else {
				left = other.center() - dir * other.radius();
			}

			self.center = nalgebra::center(&left, &right);
			self.radius = nalgebra::distance(&right, self.center());
		}
	}

	#[inline]
	fn merged(&self, other: &Self) -> Self {
		let mut result = self.clone();

		result.merge(other);

		result
	}
}

//impl RayCast for BoundingSphere {}
