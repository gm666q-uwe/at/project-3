use std::mem::swap;

use num_traits::Zero;

use crate::bounding_volume::BoundingVolume;
use crate::math::{Point, Real, Vector, DIM};
use crate::ray::{Ray, RayCast, RayIntersection};

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct AABB {
	min: Point<Real>,
	max: Point<Real>,
}

impl AABB {
	#[inline]
	pub fn extents(&self) -> Vector<Real> {
		self.max - self.min
	}

	#[inline]
	pub fn max(&self) -> &Point<Real> {
		&self.max
	}

	#[inline]
	pub fn min(&self) -> &Point<Real> {
		&self.min
	}

	pub fn new(min: Point<Real>, max: Point<Real>) -> Self {
		AABB { min, max }
	}
}

impl BoundingVolume for AABB {
	#[inline]
	fn merge(&mut self, other: &Self) {
		self.min = self.min().inf(other.min());
		self.max = self.max().sup(other.max());
	}

	#[inline]
	fn merged(&self, other: &Self) -> Self {
		Self {
			min: self.min().inf(other.min()),
			max: self.max().sup(other.max()),
		}
	}
}

impl RayCast for AABB {
	fn cast_ray(&self, ray: &Ray, mut min_toi: Real, mut max_toi: Real) -> Option<RayIntersection> {
		for i in 0..DIM {
			let denom = 1.0 / ray.direction[i];
			let mut toi0 = (self.min()[i] - ray.origin()[i]) * denom;
			let mut toi1 = (self.max()[i] - ray.origin()[i]) * denom;
			if denom < 0.0 {
				swap(&mut toi0, &mut toi1);
			}
			min_toi = toi0.max(min_toi); //if toi0 > t_min { toi0 } else { t_min };
			max_toi = toi1.min(max_toi); //if toi1 < t_max { toi1 } else { t_max };
			if max_toi <= min_toi {
				return None;
			}
		}
		Some(RayIntersection::new(0.0, Vector::zero(), false))
	}
}
