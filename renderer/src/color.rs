use nalgebra::Vector4;

use crate::math::Real;

#[inline]
fn srgb_encode(component: Real) -> Real {
	if component <= 0.0031308 {
		12.92 * component
	} else {
		1.055 * component.powf(0.4166667) - 0.055
	}
}

#[inline]
fn u16_encode(component: Real) -> u16 {
	(component * 65535.0).min(65535.0) as u16
}

#[inline]
fn u8_encode(component: Real) -> u8 {
	(component * 255.0).min(255.0) as u8
}

#[inline]
fn to_b8g8r8(color: Vector4<Real>) -> [u8; 3] {
	[u8_encode(color.z), u8_encode(color.y), u8_encode(color.x)]
}

#[inline]
fn to_b8g8r8a8(color: Vector4<Real>) -> [u8; 4] {
	[
		u8_encode(color.z),
		u8_encode(color.y),
		u8_encode(color.x),
		u8_encode(color.w),
	]
}

#[inline]
fn to_r16g16b16(color: Vector4<Real>) -> [u16; 3] {
	[u16_encode(color.x), u16_encode(color.y), u16_encode(color.z)]
}

#[inline]
fn to_r16g16b16a16(color: Vector4<Real>) -> [u16; 4] {
	[
		u16_encode(color.x),
		u16_encode(color.y),
		u16_encode(color.z),
		u16_encode(color.w),
	]
}

#[inline]
fn to_r8g8b8(color: Vector4<Real>) -> [u8; 3] {
	[u8_encode(color.x), u8_encode(color.y), u8_encode(color.z)]
}

#[inline]
fn to_r8g8b8a8(color: Vector4<Real>) -> [u8; 4] {
	[
		u8_encode(color.x),
		u8_encode(color.y),
		u8_encode(color.z),
		u8_encode(color.w),
	]
}

#[inline]
fn to_srgb(color: Vector4<Real>) -> Vector4<Real> {
	Vector4::new(
		srgb_encode(color.x),
		srgb_encode(color.y),
		srgb_encode(color.z),
		color.w,
	)
}

#[inline]
pub fn to_b8g8r8a8_srgb(color: Vector4<Real>) -> [u8; 4] {
	to_b8g8r8a8(to_srgb(color))
}

#[inline]
pub fn to_b8g8r8a8_unorm(color: Vector4<Real>) -> [u8; 4] {
	to_b8g8r8a8(color)
}

#[inline]
pub fn to_r16g16b16_srgb(color: Vector4<Real>) -> [u16; 3] {
	to_r16g16b16(to_srgb(color))
}

#[inline]
pub fn to_r8g8b8_srgb(color: Vector4<Real>) -> [u8; 3] {
	to_r8g8b8(to_srgb(color))
}

#[inline]
pub fn to_r8g8b8a8_srgb(color: Vector4<Real>) -> [u8; 4] {
	to_r8g8b8a8(to_srgb(color))
}
