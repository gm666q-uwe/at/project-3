use std::cmp::Ordering;
use std::cmp::Ordering::Less;
use std::sync::Arc;

use crate::bounding_volume::{BoundingVolume, AABB};
use crate::math::DIM;
use crate::shape::{BoundingVolumeHierarchy, Shape};

pub fn build_bounding_volume_hierarchy(objects: &mut [Arc<dyn Shape>]) -> (AABB, Arc<dyn Shape>, Arc<dyn Shape>) {
	match objects.len() {
		1 => (objects[0].compute_aabb(), objects[0].clone(), objects[0].clone()),
		2 => {
			let aabb = objects[0].compute_aabb().merged(&objects[1].compute_aabb());
			let compare = match longest_axis(&aabb) {
				0 => shape_x_compare,
				1 => shape_y_compare,
				2 => shape_z_compare,
				_ => unreachable!(),
			};

			if compare(&objects[0], &objects[1]) == Less {
				(aabb, objects[0].clone(), objects[1].clone())
			} else {
				(aabb, objects[1].clone(), objects[0].clone())
			}
		}
		len => {
			let mut aabb = objects[0].compute_aabb();
			for object in &objects[1..] {
				aabb.merge(&object.compute_aabb());
			}
			let compare = match longest_axis(&aabb) {
				0 => shape_x_compare,
				1 => shape_y_compare,
				2 => shape_z_compare,
				_ => unreachable!(),
			};

			objects.sort_by(compare);
			let mid = len / 2;

			(
				aabb,
				Arc::new(BoundingVolumeHierarchy::new(&mut objects[0..mid])),
				Arc::new(BoundingVolumeHierarchy::new(&mut objects[mid..])),
			)
		}
	}
}

fn longest_axis(aabb: &AABB) -> usize {
	let extents = aabb.extents();
	let mut axis = 0;
	let mut max = extents[axis];

	for i in 1..DIM {
		if extents[i] > max {
			axis = i;
			max = extents[i];
		}
	}

	axis
}

fn shape_x_compare(a: &Arc<dyn Shape>, b: &Arc<dyn Shape>) -> Ordering {
	a.compute_aabb()
		.min()
		.x
		.partial_cmp(&b.compute_aabb().min().x)
		.unwrap_or(Ordering::Equal)
}

fn shape_y_compare(a: &Arc<dyn Shape>, b: &Arc<dyn Shape>) -> Ordering {
	a.compute_aabb()
		.min()
		.y
		.partial_cmp(&b.compute_aabb().min().y)
		.unwrap_or(Ordering::Equal)
}

fn shape_z_compare(a: &Arc<dyn Shape>, b: &Arc<dyn Shape>) -> Ordering {
	a.compute_aabb()
		.min()
		.z
		.partial_cmp(&b.compute_aabb().min().z)
		.unwrap_or(Ordering::Equal)
}
