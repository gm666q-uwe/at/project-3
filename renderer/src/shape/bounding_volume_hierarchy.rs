use std::sync::Arc;

use crate::bounding_volume::AABB;
use crate::math::Real;
use crate::ray::{Ray, RayCast, RayIntersection};
use crate::shape::util::build_bounding_volume_hierarchy;
use crate::shape::Shape;

pub struct BoundingVolumeHierarchy {
	aabb: AABB,
	left: Arc<dyn Shape>,
	right: Arc<dyn Shape>,
}

impl BoundingVolumeHierarchy {
	#[inline]
	pub fn aabb(&self) -> &AABB {
		&self.aabb
	}

	pub fn new(objects: &mut [Arc<dyn Shape>]) -> Self {
		let (aabb, left, right) = build_bounding_volume_hierarchy(objects);
		BoundingVolumeHierarchy { aabb, left, right }
	}
}

impl RayCast for BoundingVolumeHierarchy {
	fn cast_ray(&self, ray: &Ray, min_toi: Real, max_toi: Real) -> Option<RayIntersection> {
		if let None = self.aabb.cast_ray(ray, min_toi, max_toi) {
			return None;
		}

		let left = self.left.cast_ray(ray, min_toi, max_toi);
		let right = self
			.right
			.cast_ray(ray, min_toi, if let Some(left) = left { left.toi() } else { max_toi });

		if let Some(_) = right {
			return right;
		}
		left
	}
}

impl Shape for BoundingVolumeHierarchy {
	fn compute_aabb(&self) -> AABB {
		*self.aabb()
	}
}
