pub use crate::shape::bounding_volume_hierarchy::BoundingVolumeHierarchy;
pub use crate::shape::shape::Shape;
pub use crate::shape::sphere::Sphere;
pub use crate::shape::triangle::Triangle;
pub use crate::shape::triangle_mesh::TriangleMesh;
pub use crate::shape::world::World;

mod bounding_volume_hierarchy;
mod shape;
mod sphere;
mod triangle;
mod triangle_mesh;
pub mod util;
mod world;
