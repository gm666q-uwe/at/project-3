use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::sync::Arc;

use obj::{load_obj, Obj};

use crate::bounding_volume::AABB;
use crate::math::{Point, Real};
use crate::ray::{Ray, RayCast, RayIntersection};
use crate::shape::util::build_bounding_volume_hierarchy;
use crate::shape::{Shape, Triangle};

pub struct TriangleMesh {
	aabb: AABB,
	left: Arc<dyn Shape>,
	right: Arc<dyn Shape>,
	triangles: Vec<Arc<dyn Shape>>,
}

impl TriangleMesh {
	#[inline]
	pub fn aabb(&self) -> &AABB {
		&self.aabb
	}

	pub fn new<P: AsRef<Path>>(path: P) -> Self {
		let input = BufReader::new(File::open(path.as_ref()).unwrap());
		let model: Obj = load_obj(input).unwrap();

		let triangle_count = model.indices.len() / 3;
		let mut triangles = Vec::with_capacity(triangle_count);
		for i in 0..triangle_count {
			let i = i * 3;

			let a = model.vertices[model.indices[i] as usize];
			let b = model.vertices[model.indices[i + 1] as usize];
			let c = model.vertices[model.indices[i + 2] as usize];

			triangles.push(Arc::new(Triangle::new(
				Point::new(a.position[0] as Real, a.position[1] as Real, a.position[2] as Real),
				Point::new(b.position[0] as Real, b.position[1] as Real, b.position[2] as Real),
				Point::new(c.position[0] as Real, c.position[1] as Real, c.position[2] as Real),
			)) as Arc<dyn Shape>);
		}

		let (aabb, left, right) = build_bounding_volume_hierarchy(&mut triangles[0..]);
		TriangleMesh {
			aabb,
			left,
			right,
			triangles,
		}
	}
}

impl RayCast for TriangleMesh {
	fn cast_ray(&self, ray: &Ray, min_toi: Real, max_toi: Real) -> Option<RayIntersection> {
		if let None = self.aabb.cast_ray(ray, min_toi, max_toi) {
			return None;
		}

		let left = self.left.cast_ray(ray, min_toi, max_toi);
		let right = self
			.right
			.cast_ray(ray, min_toi, if let Some(left) = left { left.toi() } else { max_toi });

		if let Some(_) = right {
			return right;
		}
		left
	}
}

impl Shape for TriangleMesh {
	fn compute_aabb(&self) -> AABB {
		*self.aabb()
	}
}
