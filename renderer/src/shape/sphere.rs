use crate::bounding_volume::AABB;
use crate::math::{Point, Real, Vector};
use crate::ray::{Ray, RayCast, RayIntersection};
use crate::shape::Shape;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Sphere {
	center: Point<Real>,
	radius: Real,
}

impl Sphere {
	#[inline]
	pub fn aabb(&self) -> AABB {
		let radius = Vector::from_element(self.radius());
		AABB::new(self.center() - radius, self.center() + radius)
	}

	#[inline]
	pub fn center(&self) -> &Point<Real> {
		&self.center
	}

	pub fn new(center: Point<Real>, radius: Real) -> Self {
		Sphere { center, radius }
	}

	#[inline]
	pub fn radius(&self) -> Real {
		self.radius
	}
}

impl RayCast for Sphere {
	fn cast_ray(&self, ray: &Ray, min_toi: f64, max_toi: f64) -> Option<RayIntersection> {
		let oc = ray.origin() - self.center();
		let a = ray.direction().magnitude_squared();
		let half_b = oc.dot(ray.direction());
		let c = oc.magnitude_squared() - self.radius() * self.radius();

		let discriminant = half_b * half_b - a * c;
		if discriminant < 0.0 {
			return None;
		}
		let sqrtd = discriminant.sqrt();

		let mut root = (-half_b - sqrtd) / a;
		if root < min_toi || max_toi < root {
			root = (-half_b + sqrtd) / a;
			if root < min_toi || max_toi < root {
				return None;
			}
		}

		let normal = (ray.point_at(root) - self.center()) / self.radius();
		let front_face = ray.direction().dot(&normal) < 0.0;
		Some(RayIntersection::new(
			root,
			if front_face { normal } else { -normal },
			front_face,
		))
	}
}

impl Shape for Sphere {
	fn compute_aabb(&self) -> AABB {
		self.aabb()
	}
}
