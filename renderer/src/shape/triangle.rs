use crate::bounding_volume::AABB;
use crate::math::{Point, Real};
use crate::ray::{Ray, RayCast, RayIntersection};
use crate::shape::Shape;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Triangle {
	a: Point<Real>,
	b: Point<Real>,
	c: Point<Real>,
}

impl Triangle {
	#[inline]
	pub fn aabb(&self) -> AABB {
		AABB::new(self.a.inf(&self.b).inf(&self.c), self.a.sup(&self.b).sup(&self.c))
	}

	pub fn new(a: Point<Real>, b: Point<Real>, c: Point<Real>) -> Self {
		Triangle { a, b, c }
	}
}

impl RayCast for Triangle {
	fn cast_ray(&self, ray: &Ray, min_toi: f64, max_toi: f64) -> Option<RayIntersection> {
		let ab = self.b - self.a;
		let ac = self.c - self.a;

		let n = ab.cross(&ac);
		let d = n.dot(ray.direction());

		if d == 0.0 {
			return None;
		}

		let ap = ray.origin() - self.a;
		let t = ap.dot(&n);

		if (t < 0.0 && d < 0.0) || (t > 0.0 && d > 0.0) {
			return None;
		}

		let d = d.abs();

		let e = -ray.direction().cross(&ap);

		let mut v;
		let mut w;
		let toi;
		let normal;

		if t < 0.0 {
			v = -ac.dot(&e);

			if v < 0.0 || v > d {
				return None;
			}

			w = ab.dot(&e);

			if w < 0.0 || v + w > d {
				return None;
			}

			let invd = 1.0 / d;
			toi = -t * invd;
			normal = -n.normalize();
			v = v * invd;
			w = w * invd;
		} else {
			v = ac.dot(&e);

			if v < 0.0 || v > d {
				return None;
			}

			w = -ab.dot(&e);

			if w < 0.0 || v + w > d {
				return None;
			}

			let invd = 1.0 / d;
			toi = t * invd;
			normal = n.normalize();
			v = v * invd;
			w = w * invd;
		}

		if toi >= min_toi && toi <= max_toi {
			Some(RayIntersection::new(toi, normal, false))
		} else {
			None
		}
	}
}

impl Shape for Triangle {
	fn compute_aabb(&self) -> AABB {
		self.aabb()
	}
}
