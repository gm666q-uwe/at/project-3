use downcast_rs::{impl_downcast, DowncastSync};

use crate::bounding_volume::AABB;
use crate::ray::RayCast;
use crate::shape::{BoundingVolumeHierarchy, Sphere, Triangle, World};

pub trait Shape: RayCast + DowncastSync {
	fn compute_aabb(&self) -> AABB;
}

impl_downcast!(sync Shape);

impl dyn Shape {
	pub fn as_bounding_volume_hierarchy(&self) -> Option<&BoundingVolumeHierarchy> {
		self.downcast_ref()
	}

	pub fn as_shape<T: Shape>(&self) -> Option<&T> {
		self.downcast_ref()
	}

	pub fn as_sphere(&self) -> Option<&Sphere> {
		self.downcast_ref()
	}

	pub fn as_triangle(&self) -> Option<&Triangle> {
		self.downcast_ref()
	}

	pub fn as_world(&self) -> Option<&World> {
		self.downcast_ref()
	}
}
