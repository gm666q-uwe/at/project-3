use std::process::exit;
use std::sync::Arc;
use std::{cmp::min, io::Write, os::unix::io::AsRawFd};

use wayland_client::{
	event_enum,
	protocol::{wl_compositor, wl_keyboard, wl_pointer, wl_seat, wl_shm},
	Display, Filter, GlobalManager,
};
use wayland_protocols::xdg_shell::client::{xdg_surface, xdg_toplevel, xdg_wm_base};

use renderer::color::{to_b8g8r8a8_srgb, to_b8g8r8a8_unorm};
use renderer::math::{Point, Real, Vector};
use renderer::shape::{Shape, Sphere, World};
use renderer::{Camera, Renderer};

event_enum!(
	Events |
	Pointer => wl_pointer::WlPointer,
	Keyboard => wl_keyboard::WlKeyboard
);

const HEIGHT: u32 = 600;
const WIDTH: u32 = 800;

fn main() {
	let aspect_ratio = WIDTH as Real / HEIGHT as Real;

	let camera = Camera::new(
		aspect_ratio,
		Point::new(0.0, 0.0, -1.0),
		Point::new(0.0, 2.0, 2.0),
		90.0,
		Vector::y_axis(),
	);

	let renderer = Renderer::new(WIDTH as usize, HEIGHT as usize, 4);

	let world = {
		let mut objects = Vec::new();

		/*let now = Instant::now();
		objects.push(Rc::new(TriangleMesh::new("bunny.obj")) as Rc<dyn Shape>);
		println!("{}", now.elapsed().as_secs());*/
		objects.push(Arc::new(Sphere::new(Point::new(0.0, 0.0, -1.0), 0.5)) as Arc<dyn Shape>);
		objects.push(Arc::new(Sphere::new(Point::new(0.0, -100.5, -1.0), 100.0)) as Arc<dyn Shape>);

		World::new(&mut objects)
	};

	let pixels = renderer.render(&camera, &world);

	let display = Display::connect_to_env().unwrap();

	let mut event_queue = display.create_event_queue();

	let attached_display = (*display).clone().attach(event_queue.token());

	let globals = GlobalManager::new(&attached_display);

	event_queue.sync_roundtrip(&mut (), |_, _, _| unreachable!()).unwrap();

	let mut tmp = tempfile::tempfile().expect("Unable to create a tempfile.");

	for pixel in &pixels {
		tmp.write_all(&to_b8g8r8a8_unorm(*pixel));
	}
	/*for i in 0..(WIDTH * HEIGHT) {
		let x = i % WIDTH;
		let y = i / WIDTH;
		let a = 0xFF;
		let r = min(((WIDTH - x) * 0xFF) / WIDTH, ((HEIGHT - y) * 0xFF) / HEIGHT);
		let g = min((x * 0xFF) / WIDTH, ((HEIGHT - y) * 0xFF) / HEIGHT);
		let b = min(((WIDTH - x) * 0xFF) / WIDTH, (y * 0xFF) / HEIGHT);
		let c = ((a << 24) + (r << 16) + (g << 8) + b).to_ne_bytes();
		tmp.write_all(&c).unwrap();
	}*/
	let _ = tmp.flush();

	let compositor = globals.instantiate_exact::<wl_compositor::WlCompositor>(1).unwrap();
	let surface = compositor.create_surface();

	let shm = globals.instantiate_exact::<wl_shm::WlShm>(1).unwrap();
	let pool = shm.create_pool(tmp.as_raw_fd(), (WIDTH * HEIGHT * 4) as i32);
	let buffer = pool.create_buffer(
		0,
		WIDTH as i32,
		HEIGHT as i32,
		(WIDTH * 4) as i32,
		wl_shm::Format::Argb8888,
	);

	let xdg_wm_base = globals
		.instantiate_exact::<xdg_wm_base::XdgWmBase>(2)
		.expect("Compositor does not support xdg_shell");

	xdg_wm_base.quick_assign(|xdg_wm_base, event, _| {
		if let xdg_wm_base::Event::Ping { serial } = event {
			xdg_wm_base.pong(serial);
		};
	});

	let xdg_surface = xdg_wm_base.get_xdg_surface(&surface);
	xdg_surface.quick_assign(move |xdg_surface, event, _| match event {
		xdg_surface::Event::Configure { serial } => {
			println!("xdg_surface (Configure)");
			xdg_surface.ack_configure(serial);
		}
		_ => unreachable!(),
	});

	let xdg_toplevel = xdg_surface.get_toplevel();
	xdg_toplevel.quick_assign(move |_, event, _| match event {
		xdg_toplevel::Event::Close => {
			exit(0);
		}
		xdg_toplevel::Event::Configure { width, height, states } => {
			println!(
				"xdg_toplevel (Configure) width: {}, height: {}, states: {:?}",
				width, height, states
			);
		}
		_ => unreachable!(),
	});
	xdg_toplevel.set_title("Simple Window".to_string());

	let common_filter = Filter::new(move |event, _, _| match event {
		Events::Pointer { event, .. } => match event {
			wl_pointer::Event::Enter {
				surface_x, surface_y, ..
			} => {
				println!("Pointer entered at ({}, {}).", surface_x, surface_y);
			}
			wl_pointer::Event::Leave { .. } => {
				println!("Pointer left.");
			}
			wl_pointer::Event::Motion {
				surface_x, surface_y, ..
			} => {
				println!("Pointer moved to ({}, {}).", surface_x, surface_y);
			}
			wl_pointer::Event::Button { button, state, .. } => {
				println!("Button {} was {:?}.", button, state);
			}
			_ => {}
		},
		Events::Keyboard { event, .. } => match event {
			wl_keyboard::Event::Enter { .. } => {
				println!("Gained keyboard focus.");
			}
			wl_keyboard::Event::Leave { .. } => {
				println!("Lost keyboard focus.");
			}
			wl_keyboard::Event::Key { key, state, .. } => {
				println!("Key with id {} was {:?}.", key, state);
			}
			_ => (),
		},
	});
	let mut pointer_created = false;
	let mut keyboard_created = false;
	globals
		.instantiate_exact::<wl_seat::WlSeat>(1)
		.unwrap()
		.quick_assign(move |seat, event, _| {
			use wayland_client::protocol::wl_seat::{Capability, Event as SeatEvent};

			if let SeatEvent::Capabilities { capabilities } = event {
				if !pointer_created && capabilities.contains(Capability::Pointer) {
					pointer_created = true;
					seat.get_pointer().assign(common_filter.clone());
				}
				if !keyboard_created && capabilities.contains(Capability::Keyboard) {
					keyboard_created = true;
					seat.get_keyboard().assign(common_filter.clone());
				}
			}
		});

	event_queue
		.sync_roundtrip(&mut (), |_, _, _| { /* we ignore unfiltered messages */ })
		.unwrap();

	surface.attach(Some(&buffer), 0, 0);
	surface.commit();

	loop {
		event_queue
			.dispatch(&mut (), |_, _, _| { /* we ignore unfiltered messages */ })
			.unwrap();
	}
}
