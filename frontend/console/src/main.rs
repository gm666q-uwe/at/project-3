use std::fs::File;
use std::io::BufWriter;
use std::str::FromStr;
use std::sync::Arc;
use std::time::Instant;

use clap::{App, Arg};
use png::chunk::ChunkType;

use renderer::color::to_r16g16b16_srgb;
use renderer::math::{Point, Real, Vector};
use renderer::shape::{Shape, Sphere, TriangleMesh, World};
use renderer::{Camera, Renderer};

#[allow(non_upper_case_globals)]
pub const cHRM: ChunkType = [b'c', b'H', b'R', b'M'];
#[allow(non_upper_case_globals)]
pub const cHRM_data: [u8; 32] = [
	0x00, 0x00, 0x7a, 0x26, 0x00, 0x00, 0x80, 0x84, 0x00, 0x00, 0xfa, 0x00, 0x00, 0x00, 0x80, 0xe8, 0x00, 0x00, 0x75,
	0x30, 0x00, 0x00, 0xea, 0x60, 0x00, 0x00, 0x3a, 0x98, 0x00, 0x00, 0x17, 0x70,
];

#[allow(non_upper_case_globals)]
pub const gAMA: ChunkType = [b'g', b'A', b'M', b'A'];
#[allow(non_upper_case_globals)]
pub const gAMA_data: [u8; 4] = [0x00, 0x00, 0xb1, 0x8f];

#[allow(non_upper_case_globals)]
pub const sRGB: ChunkType = [b's', b'R', b'G', b'B'];
#[allow(non_upper_case_globals)]
pub const sRGB_data: [u8; 1] = [0x00];

fn main() -> Result<(), <u32 as FromStr>::Err> {
	let matches = App::new("Project 3")
		.version("0.1.0")
		.author("Jan Śmiałkowski <gm666q@gm666q.space>")
		.about("Console frontend")
		.arg(
			Arg::with_name("height")
				.default_value("360")
				.help("Sets image height")
				.long("height")
				.short("h")
				.takes_value(true)
				.value_name("HEIGHT"),
		)
		.arg(
			Arg::with_name("threads")
				.default_value("16")
				.help("Sets thread count")
				.long("threads")
				.short("t")
				.takes_value(true)
				.value_name("THREADS"),
		)
		.arg(
			Arg::with_name("width")
				.default_value("640")
				.help("Sets image width")
				.long("width")
				.short("w")
				.takes_value(true)
				.value_name("WIDTH"),
		)
		.arg(
			Arg::with_name("OUTPUT")
				.help("Sets the output file to use")
				.index(1)
				.required(true),
		)
		.get_matches();

	let height = matches.value_of("height").unwrap().parse()?;
	let threads: u32 = matches.value_of("threads").unwrap().parse()?;
	let width = matches.value_of("width").unwrap().parse()?;
	let output = matches.value_of("OUTPUT").unwrap();

	let aspect_ratio = width as Real / height as Real;

	let camera = Camera::new(
		aspect_ratio,
		Point::new(0.0, 0.0, -1.0),
		Point::new(0.0, 2.0, 2.0),
		90.0,
		Vector::y_axis(),
	);

	let renderer = Renderer::new(width as usize, height as usize, 100, threads as usize);

	let mut total_time = 0.0;

	let world = {
		let mut objects = Vec::new();

		let now = Instant::now();
		objects.push(Arc::new(TriangleMesh::new("bunny.obj")) as Arc<dyn Shape>);
		let elapsed = now.elapsed().as_secs_f64();
		total_time += elapsed;
		println!("bunny.obj build time: {}", elapsed);
		objects.push(Arc::new(Sphere::new(Point::new(0.0, 0.0, -1.0), 0.5)) as Arc<dyn Shape>);
		objects.push(Arc::new(Sphere::new(Point::new(0.0, -100.5, -1.0), 100.0)) as Arc<dyn Shape>);

		World::new(&mut objects)
	};

	let ref mut w = BufWriter::new(File::create(output).unwrap());

	let mut encoder = png::Encoder::new(w, width, height);
	encoder.set_color(png::ColorType::RGB);
	encoder.set_depth(png::BitDepth::Sixteen);
	let mut writer = encoder.write_header().unwrap();
	writer.write_chunk(cHRM, &cHRM_data).unwrap();
	writer.write_chunk(gAMA, &gAMA_data).unwrap();
	writer.write_chunk(sRGB, &sRGB_data).unwrap();

	let now = Instant::now();
	let pixels = renderer.render(&camera, &world);
	let elapsed = now.elapsed().as_secs_f64();
	total_time += elapsed;
	println!("Render time: {}", elapsed);

	let now = Instant::now();
	let mut image_date = Vec::with_capacity(pixels.len() * 3);
	for pixel in &pixels {
		let color = to_r16g16b16_srgb(*pixel);
		image_date.extend_from_slice(&color[0].to_be_bytes());
		image_date.extend_from_slice(&color[1].to_be_bytes());
		image_date.extend_from_slice(&color[2].to_be_bytes());
	}
	let elapsed = now.elapsed().as_secs_f64();
	total_time += elapsed;
	println!("Color conversion time: {}", elapsed);

	let now = Instant::now();
	writer.write_image_data(&image_date).unwrap();
	let elapsed = now.elapsed().as_secs_f64();
	total_time += elapsed;
	println!("Write time: {}", elapsed);
	println!("Total time: {}", total_time);

	Ok(())
}
